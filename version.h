#if 0
# this file is valid C, Makefile and shell
# this is the only file required to update when changing the version

# https://semver.org/
VERSION_MAJOR=2
VERSION_MINOR=0
VERSION_PATCH=1
#endif

#ifndef JG_VERSION
#define JG_VERSION "2.0.1"
#endif
